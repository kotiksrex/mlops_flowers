mlops_flowers
==============================

A short description of the project.

Project Organization
------------
    ├── README.md                   <- The top-level README for developers using this project.
    ├── README_installation.md      <- Command by command description how to create a project from scratch.
    ├── data
    │   ├── processed               <- The final, canonical data sets for modeling.
    │
    ├── models                      <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks                   <- Jupyter notebooks
    │
    ├── sh                          <- shell scripts
    │
    ├── requirements.txt            <- The requirements file for reproducing the analysis environment, e.g.
    │                                  generated with `pip freeze > requirements.txt`
    │
    ├── setup.py                    <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                         <- Source code for use in this project.
    │   ├── __init__.py             <- Makes src a Python module
    │   │
    │   ├── architecture            <- Script for NN architecture
    │   │   └── arch.py
    │   │
    │   ├── optimizers              <- Script contains optimizers
    │   │   └── optimizers.py
    │   │
    │   ├── psevdo_resnet           <- Script contains psevdo resnet NN
    │   │   │
    │   │   ├── psevdo_resnet.py
    │   ├── train.py                <- Script for NN training


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
