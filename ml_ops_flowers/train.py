"""Train module."""
# import argparse
# import os
# import torch
# import torch.nn as nn  # pylint:disable=consider-using-from-import
# import torch.nn.functional as F
# import cv2  # pylint:disable=import-error
# import numpy as np
# import yaml  # type: ignore[import]
# from tqdm.autonotebook import tqdm
# from torch.cuda.amp import autocast

# from metrics import accuracy # pylint:disable=E0401

# import arch  # pylint:disable=import-error
# import optimizers  # pylint:disable=import-error


# class Dataset2Class(
#     torch.utils.data.Dataset
# ):  # pylint:disable=too-many-instance-attributes
#     """Class for custom DataSet."""

#     def __init__(  # pylint:disable=too-many-arguments
#         self,
#         path_dir1: str,
#         path_dir2: str,
#         path_dir3: str,
#         path_dir4: str,
#         path_dir5: str,
#     ) -> None:
#         """Initialize the class.

#         Parameters:
#             path_dir1 (str): path to the folder with data for the 1st class
#             path_dir2 (str): path to the folder with data for the 2nd class
#             path_dir3 (str): path to the folder with data for the 3rd class
#             path_dir4 (str): path to the folder with data for the 3rd class
#             path_dir5 (str): path to the folder with data for the 3rd class
#         """
#         super().__init__()
#         self.path_dir1 = path_dir1
#         self.path_dir2 = path_dir2
#         self.path_dir3 = path_dir3
#         self.path_dir4 = path_dir4
#         self.path_dir5 = path_dir5
#         self.dir1_list = sorted(os.listdir(path_dir1))
#         self.dir2_list = sorted(os.listdir(path_dir2))
#         self.dir3_list = sorted(os.listdir(path_dir3))
#         self.dir4_list = sorted(os.listdir(path_dir4))
#         self.dir5_list = sorted(os.listdir(path_dir5))

#     def __len__(self) -> float:
#         """Return length of the dataset."""
#         return (
#             len(self.dir1_list)
#             + len(self.dir2_list)
#             + len(self.dir3_list)
#             + len(self.dir4_list)
#             + len(self.dir5_list)
#         )

#     def __getitem__(self, idx: int) -> dict:
#         """Return image and a label of the image.

#         Parameters:
#             idx (int): index of the picture

#         Returns:
#             dict: contains image and label
#         """
#         if idx < len(self.dir1_list):
#             class_id = 0
#             img_path = os.path.join(self.path_dir1, self.dir1_list[idx])
#         if (len(self.dir1_list)) <= idx < (len(self.dir1_list) + len(self.dir2_list)):
#             class_id = 1
#             idx -= len(self.dir1_list)
#             img_path = os.path.join(self.path_dir2, self.dir2_list[idx])
#         if (
#             (len(self.dir1_list) + len(self.dir2_list))
#             <= idx
#             < (len(self.dir1_list) + len(self.dir2_list) + len(self.dir3_list))
#         ):
#             class_id = 2
#             idx -= len(self.dir1_list) + len(self.dir2_list)
#             img_path = os.path.join(self.path_dir3, self.dir3_list[idx])
#         if (
#             (len(self.dir1_list) + len(self.dir2_list) + len(self.dir3_list))
#             <= idx
#             < (
#                 len(self.dir1_list)
#                 + len(self.dir2_list)
#                 + len(self.dir3_list)
#                 + len(self.dir4_list)
#             )
#         ):
#             class_id = 3
#             idx -= len(self.dir1_list) + len(self.dir2_list) + len(self.dir3_list)
#             img_path = os.path.join(self.path_dir4, self.dir4_list[idx])
#         if (
#             (
#                 len(self.dir1_list)
#                 + len(self.dir2_list)
#                 + len(self.dir3_list)
#                 + len(self.dir4_list)
#             )
#             <= idx
#             < (
#                 len(self.dir1_list)
#                 + len(self.dir2_list)
#                 + len(self.dir3_list)
#                 + len(self.dir4_list)
#                 + len(self.dir5_list)
#             )
#         ):
#             class_id = 4
#             idx -= (
#                 len(self.dir1_list)
#                 + len(self.dir2_list)
#                 + len(self.dir3_list)
#                 + len(self.dir4_list)
#             )
#             img_path = os.path.join(self.path_dir5, self.dir5_list[idx])
#         img = cv2.imread(  # pylint:disable=redefined-outer-name, no-member
#             img_path, cv2.IMREAD_COLOR  # pylint:disable=no-member
#         )  # Загрузка изображения из файла. Первый аргумент - это путь к файлу.
#         # Второй аргумент - это загрузка изображения в формате BGR
#         img = cv2.cvtColor(  # pylint:disable=no-member
#             img, cv2.COLOR_BGR2RGB  # pylint:disable=no-member
#         )  # Конвертация изображения из BGR в RGB
#         img = img.astype(
#             np.float32
#         )  # Используем pandas метод astype для перевода в нампай флоат
#         img = img / 255.0  # нормирование
#         img = cv2.resize(  # pylint:disable=no-member
#             img,
#             (128, 128),
#             interpolation=cv2.INTER_AREA,  # pylint:disable=no-member
#         )  # Делаем resize изображения и интерполяцию
#         img = img.transpose(
#             (2, 0, 1)
#         )  # Делаем транспонироавание, так как CV2 использует HWС.
#         # Pytorch использует CHW
#         # print("Размер изображения после транспонирования ", img.shape)
#         t_img = torch.from_numpy(img)  # Создаем тензор  # pylint:disable=no-member
#         # print("Размер изображения после преобразования в тензор ", img.shape)
#         t_class_id = torch.tensor(class_id)  # pylint:disable=no-member
#         return {"img": t_img, "label": t_class_id}
#         # return img


# # PARSING
# parser = argparse.ArgumentParser()
# parser.add_argument("-option", type=str, required=True, help="Path to options file.")
# args = parser.parse_args()
# print(args.option)
# option_path = args.option

# with open(option_path, "r") as file_option:
#     option = yaml.safe_load(file_option)
# # END OF PARSING

# train_daisy_path = option["train_daisy_path"]
# train_dendelion_path = option["train_dendelion_path"]'
# train_rose_path = option["train_rose_path"]
# train_sunflower_path = option["train_sunflower_path"]
# train_tulip_path = option["train_tulip_path"]

# test_daisy_path = option["test_daisy_path"]
# test_dendelion_path = option["test_dendelion_path"]
# test_rose_path = option["test_rose_path"]
# test_sunflower_path = option["test_sunflower_path"]
# test_tulip_path = option["test_tulip_path"]

# train_ds_flowers = Dataset2Class(
#     train_daisy_path,
#     train_dendelion_path,
#     train_rose_path,
#     train_sunflower_path,
#     train_tulip_path,
# )
# test_ds_flowers = Dataset2Class(
#     test_daisy_path,
#     test_dendelion_path,
#     test_rose_path,
#     test_sunflower_path,
#     test_tulip_path,
# )

# batch_size = option["batch_size"]

# train_loader = torch.utils.data.DataLoader(
#     train_ds_flowers,
#     shuffle=True,
#     batch_size=batch_size,
#     num_workers=1,
#     drop_last=True,
# )
# test_loader = torch.utils.data.DataLoader(
#     test_ds_flowers,
#     shuffle=True,
#     batch_size=batch_size,
#     num_workers=1,
#     drop_last=False,
# )

# option_network = option["network"]
# MODEL = arch.get_network(option_network)

# LOSS_FN = nn.CrossEntropyLoss()
# option_optimizer = option["optimizer"]
# optimizer = optimizers.get_optimizer(MODEL.parameters(), option_optimizer)
# scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.6)

# DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
# MODEL = MODEL.to(DEVICE)
# LOSS_FN = LOSS_FN.to(DEVICE)

# USE_AMP = True
# scaler = torch.cuda.amp.GradScaler()

# torch.backends.cudnn.benchmark = False
# torch.backends.cudnn.deterministic = False

# epochs = option["epochs"]
# loss_epochs_list = []
# acc_epochs_list = []
# for epoch in range(epochs):
#     print("EPOCH NUMBER:", epoch + 1)
#     LOSS_VAL = 0
#     ACC_VAL = 0
#     for sample in (pbar := tqdm(train_loader)):
#         img, label = sample["img"], sample["label"]
#         label = F.one_hot(label, num_classes=5).float()
#         img = img.to(DEVICE)
#         label = label.to(DEVICE)

#         optimizer.zero_grad()
#         with autocast(USE_AMP):
#             pred = MODEL(img)  # pylint:disable=not-callable
#             # print("prediction_shape: ", pred.shape)
#             # print("label_shape: ", label.shape)
#             loss = LOSS_FN(pred, label)  # pylint:disable=not-callable

#         scaler.scale(loss).backward()
#         loss_item = loss.item()
#         LOSS_VAL += loss_item

#         scaler.step(optimizer)
#         scaler.update()

#         acc_current = accuracy(pred.cpu().float(), label.cpu().float())
#         ACC_VAL += acc_current

#         pbar.set_description(f"loss: {loss_item:.3f}\taccuracy: {acc_current:.3f}")
#     scheduler.step()
#     loss_epochs_list += [LOSS_VAL / len(train_loader)]
#     acc_epochs_list += [ACC_VAL / len(train_loader)]
#     print("LOSS:", loss_epochs_list[-1])
#     print("ACCURACY:", acc_epochs_list[-1])
# torch.save(MODEL, "models/trained_model.pt")
