"""Architecture module."""
from torch import nn
from ml_ops_flowers.psevdo_resnet import UnrealResNet  # pylint:disable=import-error


class ConvNet(nn.Module):
    """Create a convolutional neural network for image classification.

    Args:
        nn (module): Provide the neural network module.

    Attributes:
        act (LeakyReLU): Set the activation function.
        maxpool (MaxPool2d): Set the max pooling layer.
        conv0 (Conv2d): Set the first convolutional layer.
        conv1 (Conv2d): Set the second convolutional layer.
        conv2 (Conv2d): Set the third convolutional layer.
        conv3 (Conv2d): Set the fourth convolutional layer.
        adaptivepool (AdaptiveAvgPool2d): Set the adaptive pooling layer.
        flatten (Flatten): Set the flattening layer.
        linear1 (Linear): Set the first linear layer.
        linear2 (Linear): Set the second linear layer.
    """

    # Ten is reasonable in this case
    # pylint: disable=too-many-instance-attributes

    def __init__(self):
        """Initialize the CNN architecture."""
        super().__init__()
        self.act = nn.LeakyReLU(0.2)
        self.maxpool = nn.MaxPool2d(2, 2)
        self.conv0 = nn.Conv2d(3, 128, 3, stride=1, padding=0)
        self.conv1 = nn.Conv2d(128, 128, 3, stride=1, padding=0)
        self.conv2 = nn.Conv2d(128, 128, 3, stride=1, padding=0)
        self.conv3 = nn.Conv2d(128, 256, 3, stride=1, padding=0)
        self.adaptivepool = nn.AdaptiveAvgPool2d((1, 1))
        self.flatten = nn.Flatten()
        self.linear1 = nn.Linear(256, 20)
        self.linear2 = nn.Linear(20, 3)

    def forward(self, x_input_data):
        """Forward pass of the CNN.

        Args:
            x_input_data (tensor): Input tensor of shape
            (batch_size, channels, height, width).

        Returns:
            tensor: Output tensor of shape (batch_size, num_classes).
        """
        out = self.conv0(x_input_data)
        out = self.act(out)
        out = self.maxpool(out)
        out = self.conv1(out)
        out = self.act(out)
        out = self.maxpool(out)
        out = self.conv2(out)
        out = self.act(out)
        out = self.maxpool(out)
        out = self.conv3(out)
        out = self.act(out)
        out = self.adaptivepool(out)
        out = self.flatten(out)
        out = self.linear1(out)
        out = self.act(out)
        out = self.linear2(out)
        return out


def count_parameters(model):
    """Count the number of trainable parameters in a PyTorch model.

    Args:
        model (nn.Module): PyTorch model.

    Returns:
        int: Number of trainable parameters in the model.
    """
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def get_network(option_network: dict):
    """Get a PyTorch model for image classification.

    Args:
        option_network (dict): Dictionary containing options for the model
        architecture.

    Raises:
        NotImplementedError: If the specified model architecture is not
        implemented.

    Returns:
        nn.Module: PyTorch model for image classification.
    """
    arch = option_network["arch"]

    if arch == "convnet":
        network = ConvNet()
    elif arch == "psevdoresnet":
        network = UnrealResNet(option_network)
    else:
        raise NotImplementedError(f"arch [{arch}] is not implemented")
    return network
