"""Module for metrics."""
import torch.nn.functional as F


def accuracy(pred, label):  # pylint:disable=redefined-outer-name
    """Calculate model predictions.

    Parameters:
        pred: tensor (batch_size, num_classes) containing model predictions
        label: tensor (batch_size, num_classes) containing true class labels

    Returns:
        float: Percentage of model correct answers
    """
    answer = F.softmax(pred.detach()).numpy().argmax(1) == label.numpy().argmax(1)
    return answer.mean()
