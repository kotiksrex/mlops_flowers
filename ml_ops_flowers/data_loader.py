"""Train module."""
import os
import torch
import numpy as np

import cv2  # pylint:disable=import-error


class Dataset2Class(
    torch.utils.data.Dataset
):  # pylint:disable=too-many-instance-attributes
    """Class for custom DataSet."""

    def __init__(  # pylint:disable=too-many-arguments
        self,
        path_dir1: str,
        path_dir2: str,
        path_dir3: str,
        path_dir4: str,
        path_dir5: str,
    ) -> None:
        """Initialize the class.

        Parameters:
            path_dir1 (str): path to the folder with data for the 1st class
            path_dir2 (str): path to the folder with data for the 2nd class
            path_dir3 (str): path to the folder with data for the 3rd class
            path_dir4 (str): path to the folder with data for the 3rd class
            path_dir5 (str): path to the folder with data for the 3rd class
        """
        super().__init__()
        self.path_dir1 = path_dir1
        self.path_dir2 = path_dir2
        self.path_dir3 = path_dir3
        self.path_dir4 = path_dir4
        self.path_dir5 = path_dir5
        self.dir1_list = sorted(os.listdir(path_dir1))
        self.dir2_list = sorted(os.listdir(path_dir2))
        self.dir3_list = sorted(os.listdir(path_dir3))
        self.dir4_list = sorted(os.listdir(path_dir4))
        self.dir5_list = sorted(os.listdir(path_dir5))

    def __len__(self) -> float:
        """Return length of the dataset."""
        return (
            len(self.dir1_list)
            + len(self.dir2_list)
            + len(self.dir3_list)
            + len(self.dir4_list)
            + len(self.dir5_list)
        )

    def __getitem__(self, idx: int) -> dict:
        """Return image and a label of the image.

        Parameters:
            idx (int): index of the picture

        Returns:
            dict: contains image and label
        """
        if idx < len(self.dir1_list):
            class_id = 0
            img_path = os.path.join(self.path_dir1, self.dir1_list[idx])
        if (len(self.dir1_list)) <= idx < (len(self.dir1_list) + len(self.dir2_list)):
            class_id = 1
            idx -= len(self.dir1_list)
            img_path = os.path.join(self.path_dir2, self.dir2_list[idx])
        if (
            (len(self.dir1_list) + len(self.dir2_list))
            <= idx
            < (len(self.dir1_list) + len(self.dir2_list) + len(self.dir3_list))
        ):
            class_id = 2
            idx -= len(self.dir1_list) + len(self.dir2_list)
            img_path = os.path.join(self.path_dir3, self.dir3_list[idx])
        if (
            (len(self.dir1_list) + len(self.dir2_list) + len(self.dir3_list))
            <= idx
            < (
                len(self.dir1_list)
                + len(self.dir2_list)
                + len(self.dir3_list)
                + len(self.dir4_list)
            )
        ):
            class_id = 3
            idx -= len(self.dir1_list) + len(self.dir2_list) + len(self.dir3_list)
            img_path = os.path.join(self.path_dir4, self.dir4_list[idx])
        if (
            (
                len(self.dir1_list)
                + len(self.dir2_list)
                + len(self.dir3_list)
                + len(self.dir4_list)
            )
            <= idx
            < (
                len(self.dir1_list)
                + len(self.dir2_list)
                + len(self.dir3_list)
                + len(self.dir4_list)
                + len(self.dir5_list)
            )
        ):
            class_id = 4
            idx -= (
                len(self.dir1_list)
                + len(self.dir2_list)
                + len(self.dir3_list)
                + len(self.dir4_list)
            )
            img_path = os.path.join(self.path_dir5, self.dir5_list[idx])
        img = cv2.imread(  # pylint:disable=redefined-outer-name, no-member
            img_path, cv2.IMREAD_COLOR  # pylint:disable=no-member
        )  # Загрузка изображения из файла. Первый аргумент - это путь к файлу.
        # Второй аргумент - это загрузка изображения в формате BGR
        img = cv2.cvtColor(  # pylint:disable=no-member
            img, cv2.COLOR_BGR2RGB  # pylint:disable=no-member
        )  # Конвертация изображения из BGR в RGB
        img = img.astype(
            np.float32
        )  # Используем pandas метод astype для перевода в нампай флоат
        img = img / 255.0  # нормирование
        img = cv2.resize(  # pylint:disable=no-member
            img,
            (128, 128),
            interpolation=cv2.INTER_AREA,  # pylint:disable=no-member
        )  # Делаем resize изображения и интерполяцию
        img = img.transpose(
            (2, 0, 1)
        )  # Делаем транспонироавание, так как CV2 использует HWС.
        # Pytorch использует CHW
        # print("Размер изображения после транспонирования ", img.shape)
        t_img = torch.from_numpy(img)  # Создаем тензор  # pylint:disable=no-member
        # print("Размер изображения после преобразования в тензор ", img.shape)
        t_class_id = torch.tensor(class_id)  # pylint:disable=no-member
        return {"img": t_img, "label": t_class_id}
        # return img


train_daisy_path = "./data/processed/training_set/daisy"  # pylint:disable=C0103
train_dendelion_path = "./data/processed/training_set/dandelion"  # pylint:disable=C0103
train_rose_path = "./data/processed/training_set/rose"  # pylint:disable=C0103
train_sunflower_path = "./data/processed/training_set/sunflower"  # pylint:disable=C0103
train_tulip_path = "./data/processed/training_set/tulip"  # pylint:disable=C0103

test_daisy_path = "./data/processed/test_set/daisy"  # pylint:disable=C0103
test_dendelion_path = "./data/processed/test_set/dandelion"  # pylint:disable=C0103
test_rose_path = "./data/processed/test_set/rose"  # pylint:disable=C0103
test_sunflower_path = "./data/processed/test_set/sunflower"  # pylint:disable=C0103
test_tulip_path = "./data/processed/test_set/tulip"  # pylint:disable=C0103

train_ds_flowers = Dataset2Class(
    train_daisy_path,
    train_dendelion_path,
    train_rose_path,
    train_sunflower_path,
    train_tulip_path,
)
test_ds_flowers = Dataset2Class(
    test_daisy_path,
    test_dendelion_path,
    test_rose_path,
    test_sunflower_path,
    test_tulip_path,
)

# batch_size = option["batch_size"]
batch_size = 256  # pylint:disable=C0103

train_loader = torch.utils.data.DataLoader(
    train_ds_flowers,
    shuffle=True,
    batch_size=batch_size,
    num_workers=1,
    drop_last=True,
)
test_loader = torch.utils.data.DataLoader(
    test_ds_flowers,
    shuffle=True,
    batch_size=batch_size,
    num_workers=1,
    drop_last=False,
)
