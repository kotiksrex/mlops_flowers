# pylint:disable=R0801
"""Module for model training."""
import os
import hydra
import mlflow
from mlflow import log_metric, log_params

# import numpy as np asfsfwfsfwffwfw
from omegaconf import DictConfig
import torch
import torch.nn as nn  # pylint:disable=E0401, R0402
import torch.nn.functional as F
from tqdm.autonotebook import tqdm
from torch.cuda.amp import autocast
from ml_ops_flowers.arch import get_network

# import arch  # pylint:disable=E0401
from ml_ops_flowers import optimizers  # pylint:disable=E0401

# from data_loader import train_loader
from ml_ops_flowers.data_loader import Dataset2Class  # pylint:disable=E0401

from ml_ops_flowers.metrics import accuracy  # pylint:disable=E0401

MLFLOW_URI = "http://77.232.138.99:5000"
os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://77.232.138.99:9000"


@hydra.main(version_base=None, config_path="../config", config_name="training")
def train(  # pylint:disable=R0915
    cfg: DictConfig,
):  # pylint: disable=R0914
    """Train function."""
    train_daisy_path = cfg.train_daisy_path
    train_dendelion_path = cfg.train_dendelion_path
    train_rose_path = cfg.train_rose_path
    train_sunflower_path = cfg.train_sunflower_path
    train_tulip_path = cfg.train_tulip_path

    train_ds_flowers = Dataset2Class(
        train_daisy_path,
        train_dendelion_path,
        train_rose_path,
        train_sunflower_path,
        train_tulip_path,
    )

    batch_size = cfg.batch_size

    train_loader = torch.utils.data.DataLoader(
        train_ds_flowers,
        shuffle=True,
        batch_size=batch_size,
        num_workers=1,
        drop_last=True,
    )

    option_network = cfg.network
    MODEL = get_network(option_network)  # pylint:disable=C0103

    LOSS_FN = nn.CrossEntropyLoss()  # pylint:disable=C0103
    option_optimizer = cfg.optimizer
    optimizer = optimizers.get_optimizer(MODEL.parameters(), option_optimizer)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.6)

    DEVICE = "cuda" if torch.cuda.is_available() else "cpu"  # pylint:disable=C0103
    MODEL = MODEL.to(DEVICE)  # pylint:disable=C0103
    LOSS_FN = LOSS_FN.to(DEVICE)  # pylint:disable=C0103

    USE_AMP = True  # pylint:disable=C0103
    scaler = torch.cuda.amp.GradScaler()

    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = False

    epochs = cfg.epochs
    loss_epochs_list = []
    acc_epochs_list = []

    params_to_log = {}  # noqa: C408
    params_to_log["epochs"] = epochs
    params_to_log["batch_size"] = batch_size

    mlflow.set_tracking_uri(MLFLOW_URI)
    mlflow.set_experiment(cfg.experiment_name)
    mlflow.pytorch.autolog()
    with mlflow.start_run():
        # mlflow.get_artifact_uri()
        log_params(params_to_log)

        for epoch in range(epochs):
            print("EPOCH NUMBER:", epoch + 1)
            LOSS_VAL = 0  # pylint:disable=C0103
            ACC_VAL = 0  # pylint:disable=C0103
            for sample in (pbar := tqdm(train_loader)):
                img, label = sample["img"], sample["label"]
                label = F.one_hot(label, num_classes=5).float()
                img = img.to(DEVICE)
                label = label.to(DEVICE)

                optimizer.zero_grad()
                with autocast(USE_AMP):
                    pred = MODEL(img)  # pylint:disable=not-callable
                    # print("prediction_shape: ", pred.shape)
                    # print("label_shape: ", label.shape)
                    loss = LOSS_FN(pred, label)  # pylint:disable=not-callable

                scaler.scale(loss).backward()
                loss_item = loss.item()
                LOSS_VAL += loss_item  # pylint:disable=C0103

                scaler.step(optimizer)
                scaler.update()

                acc_current = accuracy(pred.cpu().float(), label.cpu().float())
                ACC_VAL += acc_current  # pylint:disable=C0103

                pbar.set_description(
                    f"loss: {loss_item:.3f}\taccuracy: {acc_current:.3f}"
                )
            scheduler.step()
            loss_epochs_list += [LOSS_VAL / len(train_loader)]
            acc_epochs_list += [ACC_VAL / len(train_loader)]
            log_metric("loss_train", loss_epochs_list[-1], step=epoch + 1)
            log_metric("acc_train", acc_epochs_list[-1], step=epoch + 1)
            print("LOSS:", loss_epochs_list[-1])
            print("ACCURACY:", acc_epochs_list[-1])
        # loss_epochs_list = loss_epochs_list[-1]
        # acc_epochs_list = acc_epochs_list[-1]
        # params_to_log["loss_epochs_list"] = loss_epochs_list
        # params_to_log["acc_epochs_list"] = acc_epochs_list
        mlflow.pytorch.log_model(
            pytorch_model=MODEL,
            registered_model_name="flowers_nn",
            artifact_path="models",
        )
        # mlflow.pytorch.log_model(MODEL, "model")
        torch.save(MODEL, "models/trained_model.pt")  # pylint:disable=C0103
        torch.save(
            MODEL.state_dict(),
            f"models/epoch{epoch + 1}.pth",
        )


if __name__ == "__main__":
    train()  # pylint:disable=E1120
