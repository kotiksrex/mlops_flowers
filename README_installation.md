### conda virt env creation

If you do not have conda installed follow the instructions below:
```
https://losst.pro/ustanovka-anaconda-v-ubuntu
```

```
conda create --name ml_ops python=3.9
```

```
conda info --envs
```

### poetry installation

```
curl -sSL https://install.python-poetry.org | python3 -
```


After installation try
```
poetry --version
```
If poetry command is not found add to .bashrc or other shell config file path to poetry bin file:
export PATH="/home/aleksei.kulikov@corp.deecoob.com/.local/bin:$PATH"`

### poetry project initialization & dependency

```
poetry init

poetry add numpy

poetry add os

poetry add torch

poetry add torchvision

poetry add random

poetry add tqdm

poetry add opencv-python

poetry add pyyaml

poetry add --dev flake8

poetry add --dev pytest
```

### directory for tests

```
mkdir tests
```

### If poetry files are already exists in the repo you need to clone the repo and use the command below

```
poetry install
```

If there is an error message first of all try to update your poetry through the command below

```
pip install poetry -U
```

Then
```
poetry install
```

### Gitlab URL

https://gitlab.com/AlexeyKulikov_ololo/mlops_flowers

### To launch training process run in the terminal command (this only with argparse library):

```
python src/train.py -option config.yml
```
### To install preßcommit hooks run this command:

```
pip install pre-commit
```

### Installation of DVC on the machine

Option 1
```
pip install dvc
```
```
conda install -c conda-forge mamba
```
```
mamba install -c conda-forge dvc-s3
```

Option 2
```
poetry add "dvc[s3]"
```


### Set up of dvc project and s3 bitbucket configuration

```
dvc init
```

### To add s3 add configuration to config file use this settings:

```
[core]
    remote = s3remote
    autostage = false

['remote "s3remote"']
    url = s3://1fb64c99-flowers
    endpointurl = https://s3.timeweb.com
    access_key_id = trololo79
    secret_access_key = 5c270ecc5b925552f2cc8756440ce63b
```

### To add file use the command below:

```
dvc add name_of_the_file
```

If you need to remove files from .git use these commands:
1. git rm -r --cached name_of_the_file
2. git commit -m "stop tracking name_of_the_file"
3. remove name_of_the_file from the .gitignore
4. dvc add name_of_the_file

```
git add dataset/.gitignore dataset/training_set.dvc dataset/test_set.dvc
```

### To add hydra to the poetry and to launch through the hydra:

```
poetry add hydra-core
```

```
python ml_ops_flowers/cli.py
```

OR

```
poetry run trainer
```

or to set any parameter from config.yaml

```
python ml_ops_flowers/cli.py batch_size=256
```

OR
```
poetry run trainer batch_size=256
```
